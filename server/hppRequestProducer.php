<?php

require_once ('vendor/autoload.php');
 
use com\realexpayments\hpp\sdk\domain\HppRequest;
use com\realexpayments\hpp\sdk\RealexHpp;
 
$amount = $_POST['AMOUNT'];
$payerRef = $_POST['PAYER_REF'];
$merchantid = "hackathon23";
$account = "internet";
$currency = "GBP";
$pmtref = "RealAID"+uniqid();
$orderid = $payerRef+ uniqid();

$hppRequest = ( new HppRequest() )
    ->addMerchantId( $merchantid )
    ->addAccount( $account )
    ->addAmount( $amount )
    ->addCurrency( $currency )
    ->addAutoSettleFlag( "1" );
    // ->addOfferSaveCard("1")
    // ->addPayerReference($payerRef)
    // ->addPayerExists("0");
    // ->addHash($hash);
	
 
$realexHpp = new RealexHpp( "secret" );
 
$requestJson = $realexHpp->requestToJson( $hppRequest );

echo $requestJson;

?>