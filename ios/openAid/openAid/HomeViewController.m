//
//  HomeViewController.m
//  openAid
//
//  Created by Andy Vizor on 11/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import "HomeViewController.h"
#import "ActivityTableViewCell.h"
#import <Parse/Parse.h>
#import <SVPullToRefresh/SVPullToRefresh.h>

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *amountLeftLabel;

@property (nonatomic, strong) HPPManager * hppManager;

@property (nonatomic, strong) NSArray *activities;
@property (nonatomic, strong) PFObject *wallet;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hppManager = [[HPPManager alloc]init];
    _hppManager.HPPRequestProducerURL =
    [NSURL URLWithString:@"https://aqueous-waters-33918.herokuapp.com/hppRequestProducer.php"];
     _hppManager.HPPURL = [NSURL URLWithString:@"https://hpp.sandbox.realexpayments.com/pay"];
     _hppManager.HPPResponseConsumerURL = [NSURL URLWithString:@"https://aqueous-waters-33918.herokuapp.com/hppRequestConsumer.php"];
    
    _hppManager.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        [self fetchWallet];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadWalletTable)
                                                 name:@"donation" object:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)reloadWalletTable
{
    [self.tableView triggerPullToRefresh];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchWallet];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hppButtonPressed:(id)sender {
    [_hppManager presentViewInViewController:self];
}

#pragma mark HPP manager delegate

-(void)HPPManagerCompletedWithResult:(NSDictionary<NSString *,NSString *> *)result
{
    
}

-(void)HPPManagerCancelled
{
    
}

-(void)HPPManagerFailedWithError:(NSError *)error
{
    
}

#pragma mark - parse stuff

- (void)fetchWallet
{
    PFUser *user = [PFUser currentUser];
    
    [user fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        self.wallet = user[@"wallet"];
        
        if (self.wallet) {
            [self.wallet fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                NSLog(@"Wallet fetched");
                PFInstallation *installation = [PFInstallation currentInstallation];
                [installation setObject:object forKey:@"activeWallet"];
                [installation saveInBackground];
                
                self.amountLeftLabel.text = [NSString stringWithFormat:@"£%.0f\rleft", [self.wallet[@"availableMoney"] floatValue]];
                
                [self fetchTransactions];
            }];
        } else {
            [self.tableView.pullToRefreshView stopAnimating];
            self.wallet = [PFObject objectWithClassName:@"Wallet"];
            self.wallet[@"availableMoney"] = @0;
            [self.wallet saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    user[@"wallet"] = self.wallet;
                    
                    PFInstallation *installation = [PFInstallation currentInstallation];
                    [installation setObject:self forKey:@"activeWallet"];
                    [installation saveInBackground];
                    
                    [user saveInBackground];
                } else {
                    // There was a problem, check error.description
                }
            }];
        }
        
    }];
}

- (void)fetchTransactions
{
//    PFRelation *transactionsRelation = self.wallet[@"Transactions"];
//    [[transactionsRelation query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error) {
//            // There was an error
//        } else {
//            self.activities = objects;
//    
//            [self.tableView reloadData];
//        }
//    }];
    PFQuery *transactionQuery = [PFQuery queryWithClassName:@"Transaction" predicate:[NSPredicate predicateWithFormat:@"wallet == %@", self.wallet]];
    [transactionQuery includeKey:@"recipient"];
    [transactionQuery orderByDescending:@"createdAt"];
    
    [transactionQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [self.tableView.pullToRefreshView stopAnimating];
        self.activities = objects;
        
        [self.tableView reloadData];
    }];
}
    
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

#pragma mark - tableview stuff

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.activities.count) {
        return 1;
    }
    return [self.activities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.activities.count) {
        UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"NoActivityTableViewCell"];
        return cell;
    }
    
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
    
    // set up with parse data.
    PFObject *aid = [self.activities objectAtIndex:indexPath.row];
    NSString *aidType = [aid[@"aidType"] lowercaseString];
    PFObject *recipient = aid[@"recipient"];
    NSString *receiverName = recipient[@"name"];
    
    PFFile *file = [recipient objectForKey:@"image"];
    // file has not been downloaded yet, we just have a handle on this file
    
    // Tell the PFImageView about your file
    cell.activityImageView.file = file;
    
    // Now tell PFImageView to download the file asynchronously
    [cell.activityImageView loadInBackground];
    
    if ([aidType isEqualToString:@"food"]) {
//        cell.activityImageView.image = [UIImage imageNamed:@"food.jpg"];
        cell.activityTitleLabel.text = [NSString stringWithFormat:@"Food for %@", receiverName];
        cell.activitySubTitleLabel.text = [NSString stringWithFormat:@"Your donation fed %@ for a day.", receiverName];
    } else if ([aidType isEqualToString:@"blankets"]) {
//        cell.activityImageView.image = [UIImage imageNamed:@"blankets.jpg"];
        cell.activityTitleLabel.text = [NSString stringWithFormat:@"Blankets for %@", receiverName];
        cell.activitySubTitleLabel.text = [NSString stringWithFormat:@"Your donation provided blankets for %@ to keep him warm.", receiverName];
    } else if ([aidType isEqualToString:@"medicine"]) {
//        cell.activityImageView.image = [UIImage imageNamed:@"tablets.png"];
        cell.activityTitleLabel.text = [NSString stringWithFormat:@"Medicine for %@", receiverName];
        cell.activitySubTitleLabel.text = [NSString stringWithFormat:@"Your donation provided medicine for %@.", receiverName];
    } else {
//        cell.activityImageView.image = nil;
        cell.activityTitleLabel.text = nil;
        cell.activitySubTitleLabel.text = nil;
    }
    
    cell.activityCostLabel.text = [NSString stringWithFormat:@"£%1.2f", [aid[@"cost"] floatValue]];
    
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMM yyyy";
    }
    
    cell.activityDateLabel.text = [dateFormatter stringFromDate:aid.createdAt];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

@end
