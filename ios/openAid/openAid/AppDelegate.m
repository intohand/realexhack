//
//  AppDelegate.m
//  openAid
//
//  Created by Andy Vizor on 11/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "HomeViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current Installation and save it to Parse</span>
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [PFPush handlePush:userInfo];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"donation" object:nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];

    if(notificationPayload)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"donation" object:nil];
    }
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    NSString *plistDirectoryPath = [[NSBundle mainBundle] pathForResource:@"parsey" ofType:@"plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

        NSDictionary *parseCreds = [[NSDictionary alloc] initWithContentsOfFile: plistDirectoryPath];
    
    [Parse setApplicationId:[parseCreds objectForKey:@"ApplicationID"] clientKey:[parseCreds objectForKey:@"ClientKey"]];
    
    if(![PFUser currentUser])
    {
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        logInViewController.delegate = self;
        
        logInViewController.logInView.usernameField.textColor = [UIColor colorWithRed:33.0/255.0f green:33.0/255.0f blue:33.0/255.0f alpha:1.0];
        logInViewController.logInView.passwordField.textColor = [UIColor colorWithRed:33.0/255.0f green:33.0/255.0f blue:33.0/255.0f alpha:1.0];

        [logInViewController.logInView.passwordForgottenButton setTitleColor:[UIColor colorWithRed:83.0/255.0f green:165.0/255.0f blue:36.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        logInViewController.logInView.backgroundColor = [UIColor whiteColor];
        [logInViewController.logInView.logInButton setBackgroundImage:nil forState:UIControlStateNormal];
        logInViewController.logInView.logInButton.backgroundColor = [UIColor colorWithRed:83.0/255.0f green:165.0/255.0f blue:36.0/255.0f alpha:1.0];
        [logInViewController.logInView.passwordForgottenButton setTitleColor:[UIColor colorWithRed:83.0/255.0f green:165.0/255.0f blue:36.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        [logInViewController.logInView.dismissButton removeFromSuperview];
        [logInViewController.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
        logInViewController.logInView.signUpButton.backgroundColor = [UIColor colorWithRed:83.0/255.0f green:165.0/255.0f blue:36.0/255.0f alpha:1.0];
        
        PFSignUpViewController *signupViewController = [[PFSignUpViewController alloc]init];
        logInViewController.signUpController = signupViewController;
        signupViewController.delegate = self;
        
        [self switchRootViewController:logInViewController animated:NO];
        
         UIImageView *iv = (UIImageView *)logInViewController.logInView.logo;
        iv.image = [UIImage imageNamed:@"smallLogo.jpg"];
        iv.frame = CGRectMake(iv.frame.origin.x, 50, iv.frame.size.width, iv.frame.size.height + (iv.frame.origin.y - 50));
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark view switching

- (UIWindow *)switchRootViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UIWindow *changeToWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    changeToWindow.rootViewController = viewController;
    
    if (animated) {
        UIView *snapshot = [self.window snapshotViewAfterScreenUpdates:NO];
        
        [changeToWindow makeKeyAndVisible];
        [changeToWindow addSubview:snapshot];
        
        [UIView animateWithDuration:0.3 animations:^{
            snapshot.alpha = 0;
            snapshot.transform = CGAffineTransformMakeScale(1.3, 1.3);
        } completion:^(BOOL finished) {
            [snapshot removeFromSuperview];
        }];
    } else {
        [changeToWindow makeKeyAndVisible];
    }
    
    // if the root view controller is presenting another view controller the window will not be released correctly.
    if (self.window.rootViewController.presentedViewController) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            self.window.rootViewController = nil;
            self.window = changeToWindow;
        }];
    } else {
        self.window.rootViewController = nil;
        self.window = changeToWindow;
    }
    
    return changeToWindow;
}

-(void)switchToHomeViewController
{
    UIStoryboard * mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeViewController * homeViewController = [mainStoryBoard instantiateInitialViewController];
    
    [self switchRootViewController:homeViewController animated:YES];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
}

#pragma mark -
#pragma mark PFLogInViewControllerDelegate

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {    
    [self switchToHomeViewController];
}

- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
}

#pragma mark -
#pragma mark PFSignUpViewControllerDelegate

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self switchToHomeViewController];
}

- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
}

@end
