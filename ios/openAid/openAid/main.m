//
//  main.m
//  openAid
//
//  Created by Andy Vizor on 11/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
