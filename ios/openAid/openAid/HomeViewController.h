//
//  HomeViewController.h
//  openAid
//
//  Created by Andy Vizor on 11/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "openAid-Bridging-Header.h"

@import RXPiOS;

@interface HomeViewController : UIViewController <HPPManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)hppButtonPressed:(id)sender;

@end
