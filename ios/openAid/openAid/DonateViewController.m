//
//  DonateViewController.m
//  openAid
//
//  Created by Tom Durrant on 12/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import "DonateViewController.h"
#import <Parse/Parse.h>

@interface DonateViewController ()

@property (weak, nonatomic) IBOutlet UIButton *TenButton;
@property (weak, nonatomic) IBOutlet UIButton *twentyFiveButton;
@property (weak, nonatomic) IBOutlet UIButton *fiftyButton;
@property (weak, nonatomic) IBOutlet UIButton *hundredButton;
@property (nonatomic, strong) HPPManager * hppManager;

@property (nonatomic, weak) UIButton *selectedButton;

@end

@implementation DonateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hppManager = [[HPPManager alloc]init];
    _hppManager.HPPRequestProducerURL =
    [NSURL URLWithString:@"https://aqueous-waters-33918.herokuapp.com/hppRequestProducer.php"];
    _hppManager.HPPURL = [NSURL URLWithString:@"https://hpp.sandbox.realexpayments.com/pay"];
    _hppManager.HPPResponseConsumerURL = [NSURL URLWithString:@"https://aqueous-waters-33918.herokuapp.com/hppRequestConsumer.php"];
    
    _hppManager.delegate = self;    

    self.TenButton.layer.cornerRadius = self.twentyFiveButton.layer.cornerRadius = self.fiftyButton.layer.cornerRadius = self.hundredButton.layer.cornerRadius = 8;
    
    self.TenButton.layer.borderWidth = self.twentyFiveButton.layer.borderWidth = self.fiftyButton.layer.borderWidth = self.hundredButton.layer.borderWidth = 1;
    
    self.TenButton.layer.borderColor = self.twentyFiveButton.layer.borderColor = self.fiftyButton.layer.borderColor = self.hundredButton.layer.borderColor = [UIColor colorWithRed:40/255.0f green:141/255.0f blue:0 alpha:1.0f].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hppButtonPressed:(id)sender {
    [_hppManager presentViewInViewController:self];
}

- (IBAction)amountButtonPressed:(UIButton *)sender
{
    if (self.selectedButton) {
        self.selectedButton.backgroundColor = [UIColor colorWithRed:215/255.0f green:204/255.0f blue:200/255.0f alpha:1.0f];
        [self.selectedButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    self.selectedButton = sender;
    
    self.selectedButton.backgroundColor = [UIColor colorWithRed:40/255.0f green:141/255.0f blue:0 alpha:1.0f];
    [self.selectedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (self.selectedButton == self.TenButton) {
        _hppManager.amount = @"10";
    } else if (self.selectedButton == self.twentyFiveButton) {
        _hppManager.amount = @"25";
    } else if (self.selectedButton == self.fiftyButton) {
        _hppManager.amount = @"50";
    } else if (self.selectedButton == self.hundredButton) {
        _hppManager.amount = @"100";
    }
    
    _hppManager.payerReference = [PFUser currentUser].objectId;
}

- (float)selectedDonationAmount
{
    if (!self.selectedButton) {
        return 0;
    }
    if (self.selectedButton == self.TenButton) {
        return 10;
    } else if (self.selectedButton == self.twentyFiveButton) {
        return 25;
    } else if (self.selectedButton == self.fiftyButton) {
        return 50;
    } else if (self.selectedButton == self.hundredButton) {
        return 100;
    }
    return 0;
}

#pragma mark HPP manager delegate

-(void)HPPManagerCompletedWithResult:(NSDictionary<NSString *,NSString *> *)result
{
    PFObject * donation = [PFObject objectWithClassName:@"Donation"];
    
    int amount = 0;
    
    if (self.selectedButton == self.TenButton) {
        amount = 10;
    } else if (self.selectedButton == self.twentyFiveButton) {
        amount = 25;
    } else if (self.selectedButton == self.fiftyButton) {
        amount = 50;
    } else if (self.selectedButton == self.hundredButton) {
        amount = 100;
    }
    
    donation[@"amount"] = [NSNumber numberWithInt:amount];
    donation[@"user"] = [PFUser currentUser];
    
    [donation saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        PFObject * wallet = [PFUser currentUser][@"wallet"];
        int oldWalletAmount = [[wallet objectForKey:@"availableMoney"]intValue];
        
        [wallet setObject:[NSNumber numberWithInt:oldWalletAmount+amount] forKey:@"availableMoney"];
        [wallet saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            UIAlertController * thanksAlert = [UIAlertController alertControllerWithTitle:@"Thank you" message:@"Your donation has been recieved" preferredStyle:UIAlertControllerStyleAlert];
            [thanksAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            [self presentViewController:thanksAlert animated:YES completion:nil];
        }];
    }];
}

-(void)HPPManagerCancelled
{
    
}

-(void)HPPManagerFailedWithError:(NSError *)error
{
    
}
@end
