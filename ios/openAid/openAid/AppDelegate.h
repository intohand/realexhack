//
//  AppDelegate.h
//  openAid
//
//  Created by Andy Vizor on 11/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIWindow *)switchRootViewController:(UIViewController *)viewController animated:(BOOL)animated;

- (void)registerForRemoteNotifications;

@end

