//
//  DonateViewController.h
//  openAid
//
//  Created by Tom Durrant on 12/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "openAid-Bridging-Header.h"

@import RXPiOS;
@interface DonateViewController : UIViewController <HPPManagerDelegate>

- (IBAction)hppButtonPressed:(id)sender;

@end
