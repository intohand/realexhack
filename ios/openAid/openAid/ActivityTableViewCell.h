//
//  ActivityTableViewCell.h
//  openAid
//
//  Created by Tom Durrant on 12/03/2016.
//  Copyright © 2016 intohand. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>

@interface ActivityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet PFImageView *activityImageView;
@property (weak, nonatomic) IBOutlet UILabel *activityTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *activitySubTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDateLabel;

@end
