package com.intohand.realex.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intohand.realex.Const;
import com.intohand.realex.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.postquantum.pqcheck.android.PQCheckActivity;
import com.postquantum.pqcheck.clientlib.response.ApiKey;

import java.text.NumberFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Class: com.intohand.realex.activity.RedeemActivity
 * Project: android
 * Created Date: 12/03/16 01:49
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class RedeemActivity extends BaseActivity implements OnMapReadyCallback
{
	public static final String TAG = "RedeemActivity";
	private static final double START_RADIUS = 1;

	public static Intent createIntent(Context context){
		return new Intent(context, RedeemActivity.class);
	}

	private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);
	private static int B1 = 21499;
	private static int B2 = 27570;
	//private static final Region BEACON1 = new Region("b1", null, 1413, 21499);
	//private static final Region BEACON2 = new Region("b2", null, 5525, 27570);
	private BeaconManager beaconManager;

	double m1;
	double m2;

	@Bind(R.id.credit)
	TextView credit;

	@Bind(R.id.img)
	ImageView img;

	@Bind(R.id.payButton)
	Button payButton;

	private GoogleMap mMap;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.redeem);
		ButterKnife.bind(this);

		setTitle("My Title");

		payButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view){
				pay();
			}
		});

		beaconManager = new BeaconManager(this);
		beaconManager.setRangingListener(new BeaconManager.RangingListener() {
			@Override
			public void onBeaconsDiscovered(Region region, final List<Beacon> rangedBeacons) {
				Log.d(TAG, "onBeaconsDiscovered(" + "region:" + region + ", rangedBeacons:" + rangedBeacons + "" + ")");

				for(int i = 0; i < rangedBeacons.size(); i++) {
					Beacon b =  rangedBeacons.get(i);
					boolean change = false;
					if(b.getMinor() == B1){
						m1 = Utils.computeAccuracy(b);
						change = true;
					}
					if(b.getMinor() == B2){
						m2 = Utils.computeAccuracy(b);
						change = true;
					}
					if(change)
						setDistance(m1, m2);
				}
			}
		});

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
	}

	LatLng blanketLoc = new LatLng(51.532765f, -0.122201f);
	LatLng foodLoc = new LatLng(51.533686f, -0.122395f);
	Marker bm;
	Marker fm;

	@Override
	public void onMapReady(GoogleMap googleMap){
		mMap = googleMap;

		mMap.setMyLocationEnabled(true);
		mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener()
		{
			@Override
			public void onMyLocationChange(Location location){
				LatLngBounds.Builder b = LatLngBounds.builder();
				b.include(new LatLng(location.getLatitude(), location.getLongitude()));
				b.include(blanketLoc);
				b.include(foodLoc);
				LatLngBounds bounds = b.build();
				CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, toPixels(24));
				mMap.animateCamera(update);
				bm.showInfoWindow();
				fm.showInfoWindow();
			}
		});

		// Add a marker in Sydney and move the camera
		bm = mMap.addMarker(new MarkerOptions().position(blanketLoc).title("Blankets"));
		fm = mMap.addMarker(new MarkerOptions().position(foodLoc).title("Food"));

		mMap.moveCamera(CameraUpdateFactory.newLatLng(foodLoc));
	}

	private void pay(){
		if(!Const.PQ_ENABLED){
			completeTransaction();
			return;
		}
		//"https://api-pqcheck.post-quantum.com"
		Intent intent = new Intent(getCtx(), PQCheckActivity.class);
		intent.setAction(PQCheckActivity.ACTION_AUTHORISE);
		intent.putExtra(PQCheckActivity.EXTRA_API_KEY, new ApiKey(Const.SERVER_API_KEY, Const.SERVER_SECRET));
		//intent.setData(Uri.parse(approvalUri));
		intent.putExtra(PQCheckActivity.EXTRA_REFERENCE, "MyRef");
		intent.putExtra(PQCheckActivity.EXTRA_USER_IDENTIFIER, getString(Const.USERNAME, ""));
		intent.putExtra(PQCheckActivity.EXTRA_HASH, "17292913");
		intent.putExtra(PQCheckActivity.EXTRA_SUMMARY, "493");
		intent.putExtra(PQCheckActivity.EXTRA_PACE_USER, true);
		startActivityForResult(intent, Const.VERIFICATION_REQUEST);
	}

	private String p(double d){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		return nf.format(d);
	}

	State state;
	enum State{NEAR_FOOD, NEAR_BLANKET, IDLE}
	private void setDistance(double metersB1, double metersB2){
		Log.v(TAG, "setDistance(" + "metersB1:" + p(metersB1) + ", metersB2:" + p(metersB2) + "" + ")");

		double m = Math.min(metersB1, metersB2);

		if(m < START_RADIUS){
			if(metersB1 < metersB2)
				setState(State.NEAR_FOOD);
			else
				setState(State.NEAR_BLANKET);
		}
		else
			setState(State.IDLE);
	}

	int foodBalance = 3;
	int blanketBalance = 2;
	public void setState(State state){
		if(this.state == state) return;
		Log.d(TAG, "New state: " + state + "metersB1:" + p(m1) + ", metersB2:" + p(m2));
		this.state = state;
		updateViewState();
	}
	private void updateViewState(){
		h.post(new Runnable()
		{
			@Override
			public void run(){
				switch(state){
					case IDLE:
						setTitle(R.string.app_name);
						credit.setText("Food and blankets available");
						fade(img, 0);
						break;
					case NEAR_FOOD:
						setTitle("Food");
						if(foodBalance > 1)
							credit.setText(foodBalance +" meals available");
						else if(foodBalance == 1)
							credit.setText("1 meal available");
						else
							credit.setText("Come back tomorrow");
						fade(img, R.drawable.soup);
						break;
					case NEAR_BLANKET:
						setTitle("Blankets");
						if(blanketBalance > 1)
							credit.setText(blanketBalance +" blankets available");
						else if(blanketBalance == 1)
							credit.setText("1 blanket available");
						else
							credit.setText("Come back tomorrow");

						fade(img, R.drawable.blanket);
						break;
				}
			}
		});
	}

	int currentImgRes = 0;
	private void fade(final ImageView img, final int imgRes){
		if(imgRes == 0){
			//fade out
			img.animate().alpha(0).start();
		}
		else if(currentImgRes == 0){
			//fade in
			img.setImageResource(imgRes);
			img.animate().alpha(1).start();
		}
		else{
			//crossfade
			img.animate().alpha(0).withEndAction(new Runnable()
			{
				@Override
				public void run(){
					img.setImageResource(imgRes);
					img.animate().alpha(1).start();
				}
			}).start();
		}
		payButton.animate().alpha(imgRes == 0 ? 0 : 1).start();
		currentImgRes = imgRes;
	}


	@Override
	protected void onResume(){
		super.onResume();
		beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
			@Override
			public void onServiceReady() {
				beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
				//beaconManager.startRanging(BEACON1);
				//beaconManager.startRanging(BEACON2);
			}
		});
	}

	@Override
	protected void onPause(){
		beaconManager.disconnect();

		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult(" + "requestCode:" + requestCode + ", resultCode:" + resultCode + ", data:" + print(data) + "" + ")");

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Const.VERIFICATION_REQUEST) {
			switch (resultCode) {
				case Activity.RESULT_OK:
					completeTransaction();
					break;
				case PQCheckActivity.RESULT_CLIENT_ERROR:
				case PQCheckActivity.RESULT_SERVER_ERROR:
					notify(data.getExtras().getString(PQCheckActivity.EXTRA_RESULT_ERROR_MESSAGE));
					break;
				case PQCheckActivity.RESULT_TIMEOUT:
					notify("Session expired");
					break;
			}
		}
	}

	private void completeTransaction(){
		ParseObject trans = new ParseObject("Transaction");
		trans.put("cost", 5);
		trans.put("aidType", state == State.NEAR_BLANKET ? "Blankets" : "Food");
		ParseObject recipient = ParseObject.createWithoutData("recipient", getString(Const.PID, null));
		trans.put("recipient", recipient);
		trans.saveInBackground(new SaveCallback()
		{
			@Override
			public void done(ParseException e){
				if(e == null){
					updateBalance(state);
					toast("Purchased");
				} else {
					showError(e);
				}
			}
		});
	}

	private void updateBalance(State state){
		if(state == State.NEAR_BLANKET)
			blanketBalance--;
		else
			foodBalance--;
		updateViewState();
	}
}
