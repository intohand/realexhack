package com.intohand.realex.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.intohand.realex.Const;
import com.intohand.realex.R;
import com.postquantum.pqcheck.android.PQCheckActivity;
import com.postquantum.pqcheck.clientlib.response.ApiKey;

import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Class: com.intohand.realex.activity.RegistrationActivity
 * Project: android
 * Created Date: 11/03/16 23:02
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class RegistrationActivity extends BaseActivity
{
	public static final String TAG = "RegistrationActivity";


	public static Intent createIntent(Context context){
		return new Intent(context, RegistrationActivity.class);
	}

	@Bind(R.id.name)
	EditText name;

	@Bind(R.id.enrolButton)
	Button enrolButton;


	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);

		setTitle(R.string.app_name);

		ButterKnife.bind(this);

		enrolButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view){
				enrol();
			}
		});
	}


	private void enrol() {

		final String myName = name.getText().toString();
		if("".equals(myName)){
			toast("Please enter your name");
			return;
		}
		set(Const.USERNAME, myName);

		if(!Const.PQ_ENABLED){
			startActivity(PictureActivity.createIntent(getCtx()));
			return;
		}

		notify(getString(R.string.pq_intro), new Runnable()
		{
			@Override
			public void run(){
				//String enrolmentUri = "";
				//String transcript = "123456";
				Intent intent = new Intent(getCtx(), PQCheckActivity.class);
				intent.setAction(PQCheckActivity.ACTION_ENROL);
				intent.putExtra(PQCheckActivity.EXTRA_API_KEY, new ApiKey(Const.SERVER_API_KEY, Const.SERVER_SECRET));
				//intent.setData(Uri.parse(enrolmentUri));
				//intent.putExtra(PQCheckActivity.EXTRA_TRANSCRIPT, transcript);
				intent.putExtra(PQCheckActivity.EXTRA_REFERENCE, "MyRef");
				intent.putExtra(PQCheckActivity.EXTRA_USER_IDENTIFIER, myName);

				intent.putExtra(PQCheckActivity.EXTRA_PACE_USER, true);
				startActivityForResult(intent, Const.ENROLMENT_REQUEST);
			}
		});


	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult(" + "requestCode:" + requestCode + ", resultCode:" + resultCode + ", data:" + print(data) + "" + ")");

		if (requestCode == Const.ENROLMENT_REQUEST) {
			switch (resultCode) {
				case Activity.RESULT_OK:
					startActivity(PictureActivity.createIntent(getCtx()));
					break;
				case PQCheckActivity.RESULT_CLIENT_ERROR: {
					Bundle extras = data.getExtras();
					String error = extras.getString(PQCheckActivity.EXTRA_RESULT_ERROR_MESSAGE);
					notify("Check your network connection");
					break;
				}
				case PQCheckActivity.RESULT_SERVER_ERROR: {
					Bundle extras = data.getExtras();
					String error = extras.getString(PQCheckActivity.EXTRA_RESULT_ERROR_MESSAGE);
					notify("Error: "+error);
					break;
				}
				case PQCheckActivity.RESULT_TIMEOUT:
					notify("Expired");
					break;
			}
		}
		else
			super.onActivityResult(requestCode, resultCode, data);
	}
}
