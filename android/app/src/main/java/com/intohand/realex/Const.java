package com.intohand.realex;

import java.util.UUID;

/**
 * Class: com.intohand.realex.Const
 * Project: android
 * Created Date: 11/03/16 23:01
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class Const
{
	public static final String USERNAME = "Username";

	public static final UUID SERVER_API_KEY = UUID.fromString(BuildConfig.PQ_API_KEY);
	public static final String SERVER_SECRET = BuildConfig.PQ_SECRET;

	public static final String APP_ID = BuildConfig.PARSE_APP_ID;
	public static final String CLIENT_KEY = BuildConfig.PARSE_CLIENT_KEY;

	public static final String PID = "ParseID";

	public static final int ENROLMENT_REQUEST = 123;
	public static final int VERIFICATION_REQUEST = 124;
	public static final int CAMERA_PHOTO_REQUEST_CODE = 125;

	public static final boolean PQ_ENABLED = true;

	//public static URI BASE_URL = URI.create("https://stable-beta-api-pqcheck.post-quantum.com");

}
