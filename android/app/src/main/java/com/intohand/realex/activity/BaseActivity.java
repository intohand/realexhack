package com.intohand.realex.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.intohand.realex.R;

import java.util.Iterator;
import java.util.Set;

/**
 * Class: com.intohand.realex.activity.BaseActivity
 * Project: android
 * Created Date: 11/03/16 23:10
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class BaseActivity extends AppCompatActivity
{
	protected String TAG = "BaseActivity";

	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	Handler h = new Handler();

	private SharedPreferences getPrefs(){
		if(prefs == null)
			prefs = PreferenceManager.getDefaultSharedPreferences(this);
		return prefs;
	}

	private SharedPreferences.Editor getEditor(){
		if(editor == null)
			editor = getPrefs().edit();
		return editor;
	}

	public Context getCtx(){
		return this;
	}

	public void set(String key, String value){
		if(value == null){
			if(exists(key))
				delete(key);
		} else {
			SharedPreferences.Editor prefs = getEditor();
			prefs.putString(key, value);
			prefs.commit();
		}
	}

	public String getString(String key, String def){
		return getPrefs().getString(key, def);
	}

	public boolean exists(String key){
		SharedPreferences prefs = getPrefs();
		boolean result = prefs.contains(key);
		return result;
	}

	public void delete(String key){
		SharedPreferences.Editor prefs = getEditor();
		prefs.remove(key);
		prefs.commit();
	}

	public void toast(final CharSequence str){
		Log.i(TAG, str.toString());
		runOnUiThread(new Runnable()
		{
			public void run(){
				Toast.makeText(getCtx(), str, Toast.LENGTH_SHORT).show();
			}
		});

	}

	protected void showError(Throwable t){
		notify("Error: " + t.getMessage());
		Log.w(TAG, t);
	}

	public static String print(Intent intent){
		if(intent == null)
			return "Intent: null";

		String result = "Intent: " + intent.toString();
		try {
			Bundle b = intent.getExtras();
			if(b != null && !b.isEmpty()){
				result += "\nExtras: ";
				Set<String> keys = b.keySet();
				Iterator<String> it = keys.iterator();
				while(it.hasNext()) {
					String key = it.next();
					Object o = b.get(key);
					String ostring = o.toString();
					result += "\nkey(" + key + ") val(" + ostring + ")";
				}
			}
		}
		catch(Exception e) {
			result += "\nError: " + e.getMessage();
		}
		return result;
	}

	ProgressDialog pd;

	public void showProgress(final boolean inProg){
		showProgress(inProg, null);
	}

	public void showProgress(final boolean inProg, final String msg){
		Log.d(TAG, "showProgress(" + inProg + "," + msg + ")");
		h.post(new Runnable()
		{
			@Override
			public void run(){
				try {
					if(pd == null){
						pd = new ProgressDialog(getCtx());
						pd.setIndeterminate(true);
						pd.setCancelable(false);
					}
					if(msg == null)
						pd.setMessage("Loading...");
					else
						pd.setMessage(msg);

					if(inProg){
						if(!pd.isShowing()){
							pd.show();
						}
					} else {
						if(pd.isShowing()){
							pd.dismiss();
						}
					}
				}
				catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
				}
			}
		});
	}

	public void notify(String msg){
		notify(msg, null);
	}
	public void notify(String msg, final Runnable endAction){
		AlertDialog.Builder builder = new AlertDialog.Builder(getCtx());
		builder.setMessage(msg);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which){
				if(endAction != null)
					endAction.run();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public int toPixels(float dips) {
		return toPixels(dips, getDpi());
	}
	public int toPixels(float dips, int dpi) {
		return (int) (dips * ((float) dpi / 160));//px = dp * (dpi / 160)
	}
	int dpi;

	public int getDpi(){
		try {
			if(dpi == 0){
				DisplayMetrics metrics = new DisplayMetrics();
				WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
				wm.getDefaultDisplay().getMetrics(metrics);
				//if(logger.isDebug()) logger.trace("using density: " + metrics.densityDpi + " from metrics: " + metrics);
				dpi = metrics.densityDpi;
			}
			return dpi;
		}
		catch (Exception e) {
			Log.w(TAG, "Problem getting DPI - returning 1:1", e);
			return 160;
		}
	}
}
