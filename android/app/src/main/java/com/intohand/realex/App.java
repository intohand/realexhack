package com.intohand.realex;

import android.app.Application;

import com.parse.Parse;

/**
 * Class: com.intohand.realex.App
 * Project: android
 * Created Date: 12/03/16 04:19
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class App extends Application
{
	@Override
	public void onCreate(){
		super.onCreate();
		Parse.initialize(this, Const.APP_ID, Const.CLIENT_KEY);
	}
}
