package com.intohand.realex.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.intohand.realex.Const;
import com.intohand.realex.R;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Class: com.intohand.realex.activity.PictureActivity
 * Project: android
 * Created Date: 12/03/16 03:57
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class PictureActivity extends BaseActivity
{
	@Bind(R.id.photoButton)
	Button photoButton;

	File f;

	public static Intent createIntent(Context context){
		return new Intent(context, PictureActivity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.picture);
		ButterKnife.bind(this);

		f = new File(getExternalFilesDir(null) + "/tmp-img.jpg");
		Log.d(TAG, "f: " + f + " exists:" + f.exists() + " len:" + f.length());

		photoButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view){
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				Uri photoUri = Uri.fromFile(f);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
				intent.putExtra("android.intent.extras.CAMERA_FACING", 1);

				startActivityForResult(intent, Const.CAMERA_PHOTO_REQUEST_CODE);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.CAMERA_PHOTO_REQUEST_CODE){
			Log.d(TAG, "data: " + print(data));

			saveUser();
		}
	}

	private void saveUser(){
		Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
		// Convert it to byte
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Compress image to lower quality scale 1 - 100
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] image = stream.toByteArray();

		// Create the ParseFile
		final ParseFile file = new ParseFile(f.getName(), image);

		showProgress(true, "Uploading...");
		file.saveInBackground(new SaveCallback()
			  {
				  public void done(ParseException e){
					  if(e == null){
						  Log.d(TAG, "File saved: "+file);
						  showProgress(true, "Registering...");
						  final ParseObject recipient = new ParseObject("recipient");
						  recipient.put("image", file);
						  //recipient.add("uuid", UUID.randomUUID().toString());
						  recipient.put("name", getString(Const.USERNAME, "--"));
						  Log.d(TAG, "about to save: " + recipient);
						  recipient.saveInBackground(new SaveCallback()
						  {
							  @Override
							  public void done(ParseException e){
								  if(e == null){
									  String pid = recipient.getObjectId();
									  showProgress(false);
									  set(Const.PID, pid);
									  gotoStart();
								  } else {
									  showProgress(false);
									  showError(e);
								  }
							  }
						  });

					  } else {
						  showProgress(false);
						  showError(e);
					  }
				  }
			  }
		);
	}

	private void gotoStart(){
		startActivity(StartActivity.createIntent(getCtx()));
		finish();
	}
}
