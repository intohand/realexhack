package com.intohand.realex.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.intohand.realex.Const;
import com.intohand.realex.R;

/**
 * Class: com.intohand.realex.activity.StartActivity
 * Project: RealEx Hack
 * Created Date: 11/03/16 22:48
 *
 * @author <a href="mailto:elliot@intohand.com">Elliot Long</a>
 *         Copyright (c) 2014 Intohand Ltd. All rights reserved.
 */

public class StartActivity extends BaseActivity
{
	public static Intent createIntent(Context context){
		return new Intent(context, StartActivity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		Intent i;
		if(exists(Const.PID)){
			//go to redeem activity
			i = RedeemActivity.createIntent(getCtx());
		}
		else{
			//go to registration
			i = RegistrationActivity.createIntent(getCtx());
		}
		//i = PictureActivity.createIntent(getCtx());
		startActivity(i);
	}
}
