
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.beforeSave("Transaction", function(request, response) {
  if (request.object.isNew())
  {
		var Wallet = Parse.Object.extend("Wallet");
		var pointerToWallet = new Wallet();
		pointerToWallet.id = "hzCQe6c3W5";
  		request.object.set("wallet", pointerToWallet);
  		response.success();
  }
  else
  {
  		response.success();
  }
})

Parse.Cloud.afterSave("Transaction", function(request)
{
	var transactionCost = request.object.get("cost")

		var query = new Parse.Query("Wallet");
		query.equalTo("objectId", "hzCQe6c3W5");
		query.include("user");
		
		query.first({
  			success: function(walletToDeduct) {
  				var oldMoney = walletToDeduct.get("availableMoney");
				var newMoney = oldMoney-transactionCost;
				walletToDeduct.set("availableMoney", newMoney);
				walletToDeduct.save();
								
				var recipientId = request.object.get("recipient");
				console.log("fetching " + recipientId);
				var query = new Parse.Query("recipient");
				query.equalTo("objectId", recipientId.id);
				query.first({
  					success: function(recipient) {
  						if(recipient)
  						{
							console.log("recip" + recipient);

							var alertString = recipient.get("name") + " has received a donation from you!";
							console.log(alertString);
							//Push notification
							var sendquery = new Parse.Query(Parse.Installation);
							sendquery.equalTo('activeWallet',walletToDeduct);
						
							Parse.Push.send(
							{
								where: sendquery,
								data: {
									alert: alertString,
									badge: "Increment"
									// "action-loc-key": "View"
								}
							}, 
							{
								success: function() {
									console.log("Did send notification");
								},
								error: function(error) {
									console.log("failed to send notification");
								}
							});
						}
						else
						{
							console.log("nooooooo");
						}
  					},
  					error: function(error) {
  						console.log("failed to send notification");
					}
				});
  			},
  			error: function(error) {
  				response.error("Wallet not deducted");
  			}
		});
})